<?php

namespace App\Modules\Invoices\Domain\Repositories;

use App\Modules\Invoices\Domain\Entities\Invoice;
use App\Modules\Invoices\Infrastructure\Database\Exceptions\EntityNotFoundException;

interface InvoiceRepositoryInterface
{
    /**
     * @return Invoice[]
     */
    public function getAll(): array;

    /**
     * @param string $getInvoiceNumber
     * @return Invoice
     * @throws EntityNotFoundException
     */
    public function getByNumber(string $getInvoiceNumber): Invoice;

    /**
     * @param Invoice $approve
     * @return void
     */
    public function update(Invoice $approve): void;
}

<?php

namespace App\Modules\Invoices\Domain\Mappers;

use App\Domain\Enums\StatusEnum;
use App\Modules\Invoices\Domain\Entities\Company;
use App\Modules\Invoices\Domain\Entities\Invoice;
use App\Modules\Invoices\Domain\ValueObjects\AddressVO;
use App\Modules\Invoices\Domain\ValueObjects\InvoiceProductVO;

class InvoiceMapper
{
    /**
     * @param \stdClass $row
     * @param \stdClass[] $products
     * @return Invoice
     * @throws \Exception
     */
    public function map(\stdClass $row, array $products): Invoice
    {
        return new Invoice(
            $row->invoice_id,
            $row->invoice_number,
            (new \DateTime($row->invoice_date))->setTime(0, 0),
            (new \DateTime($row->invoice_due_date))->setTime(0, 0),
            new Company(
                $row->company_id,
                $row->company_name,
                $row->company_phone,
                $row->company_email,
                new AddressVO(
                    $row->company_street,
                    $row->company_city,
                    $row->company_zip,
                )
            ),
            StatusEnum::tryFrom($row->invoice_status),
            new \DateTime($row->invoice_created_at),
            new \DateTime($row->invoice_updated_at),
            array_map(static fn(\stdClass $row) => new InvoiceProductVO(
                $row->product_name,
                $row->product_quantity,
                $row->product_price,
                $row->product_currency,
            ), $products),
        );
    }
}

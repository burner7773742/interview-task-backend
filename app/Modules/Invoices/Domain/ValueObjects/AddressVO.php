<?php

namespace App\Modules\Invoices\Domain\ValueObjects;

readonly class AddressVO implements \JsonSerializable
{
    public function __construct(
        private string $street,
        private string $city,
        private string $zip,
    ) {
    }

    public function jsonSerialize(): array
    {
        return [
            'street' => $this->street,
            'city' => $this->city,
            'zip' => $this->zip,
        ];
    }
}

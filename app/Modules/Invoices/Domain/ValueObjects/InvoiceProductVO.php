<?php

namespace App\Modules\Invoices\Domain\ValueObjects;

class InvoiceProductVO implements \JsonSerializable
{
    public function __construct(
        private string $name,
        private int $quantity,
        private int $price,
        private string $currency,
    ) {
    }

    public function jsonSerialize(): array
    {
        return [
            'name' => $this->name,
            'quantity' => $this->quantity,
            'price' => $this->price,
            'currency' => $this->currency,
        ];
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function getPrice(): int
    {
        return $this->price;
    }
}

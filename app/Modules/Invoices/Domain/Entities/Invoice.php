<?php

namespace App\Modules\Invoices\Domain\Entities;

use App\Domain\Enums\StatusEnum;
use App\Modules\Invoices\Domain\ValueObjects\InvoiceProductVO;

class Invoice implements \JsonSerializable
{
    /**
     * @param string $id
     * @param string $number
     * @param \DateTime $date
     * @param \DateTime $dueDate
     * @param Company $company
     * @param StatusEnum $status
     * @param \DateTime $createdAt
     * @param \DateTime $updatedAt
     * @param InvoiceProductVO[] $products
     */
    public function __construct(
        private string $id,
        private string $number,
        private \DateTime $date,
        private \DateTime $dueDate,
        private Company $company,
        private StatusEnum $status,
        private \DateTime $createdAt,
        private \DateTime $updatedAt,
        private array $products,
    ) {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getNumber(): string
    {
        return $this->number;
    }

    public function getDate(): \DateTime
    {
        return $this->date;
    }

    public function getDueDate(): \DateTime
    {
        return $this->dueDate;
    }

    public function getCompany(): Company
    {
        return $this->company;
    }

    public function getStatus(): StatusEnum
    {
        return $this->status;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    public function jsonSerialize(): mixed
    {
        return [
            'id' => $this->id,
            'number' => $this->number,
            'date' => $this->date->format('Y-m-d'),
            'dueDate' => $this->dueDate->format('Y-m-d'),
            'company' => $this->company,
            'status' => $this->status,
            'createdAt' => $this->createdAt->format('Y-m-d H:i:s'),
            'updatedAt' => $this->updatedAt->format('Y-m-d H:i:s'),
            'products' => $this->products,
            'total' => $this->getTotal()
        ];
    }

    public function approve(): self
    {
        $this->status = StatusEnum::APPROVED;
        return $this;
    }

    public function reject(): self
    {
        $this->status = StatusEnum::REJECTED;
        return $this;
    }

    /**
     * @return InvoiceProductVO[]
     */
    public function getProducts(): array
    {
        return $this->products;
    }

    private function getTotal(): int
    {
        $total = 0;

        foreach ($this->products as $product) {
            $total += $product->getQuantity() * $product->getPrice();
        }

        return $total;
    }
}

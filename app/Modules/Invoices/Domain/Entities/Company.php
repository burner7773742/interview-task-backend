<?php

namespace App\Modules\Invoices\Domain\Entities;

use App\Modules\Invoices\Domain\ValueObjects\AddressVO;

class Company implements \JsonSerializable
{
    public function __construct(
        private string $id,
        private string $name,
        private string $phone,
        private string $email,
        private AddressVO $addressVO,
    ) {
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => $this->email,
            'address' => $this->addressVO
        ];
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }
}

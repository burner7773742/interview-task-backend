<?php

namespace App\Modules\Invoices\Application\Api\Actions\ShowInvoiceData;

use App\Modules\Invoices\Domain\Entities\Invoice;
use App\Modules\Invoices\Domain\Repositories\InvoiceRepositoryInterface;

readonly class ShowInvoiceDataService
{
    public function __construct(private InvoiceRepositoryInterface $invoiceRepository)
    {
    }

    /**
     * @return Invoice[]
     */
    public function getAll(): array
    {
        return $this->invoiceRepository->getAll();
    }
}

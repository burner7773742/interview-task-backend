<?php

namespace App\Modules\Invoices\Application\Api\Actions\ShowInvoiceData;

use App\Modules\Invoices\Application\Api\Actions\ActionInterface;
use App\Modules\Invoices\Domain\Entities\Invoice;
use App\Modules\Invoices\Infrastructure\Http\Response\ResponseInterface;
use App\Modules\Invoices\Infrastructure\Http\Response\SuccessResponse;

readonly class ShowInvoiceDataAction implements ActionInterface
{
    public function __construct(private ShowInvoiceDataService $showInvoiceDataService)
    {
    }

    /**
     * @return Invoice[]
     */
    public function execute(): ResponseInterface
    {
        return new SuccessResponse(['invoices' => $this->showInvoiceDataService->getAll()]);
    }
}

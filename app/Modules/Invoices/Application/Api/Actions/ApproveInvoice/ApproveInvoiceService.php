<?php

namespace App\Modules\Invoices\Application\Api\Actions\ApproveInvoice;

use App\Modules\Approval\Api\ApprovalFacadeInterface;
use App\Modules\Approval\Api\Dto\ApprovalDto;
use App\Modules\Invoices\Domain\Repositories\InvoiceRepositoryInterface;
use App\Modules\Invoices\Infrastructure\Database\Exceptions\EntityNotFoundException;
use Ramsey\Uuid\Uuid;

readonly class ApproveInvoiceService
{
    public function __construct(
        private InvoiceRepositoryInterface $invoiceRepository,
        private ApprovalFacadeInterface $approvalFacade
    ) {
    }

    /**
     * @throws EntityNotFoundException
     */
    public function approveInvoice(ApproveInvoiceDTO $approveInvoiceDTO): bool
    {
        $invoice = $this->invoiceRepository->getByNumber($approveInvoiceDTO->getInvoiceNumber());

        $this->approvalFacade->approve(
            new ApprovalDto(
                Uuid::fromString($invoice->getId()),
                $invoice->getStatus(),
                'Foo'
            )
        );

        $this->invoiceRepository->update($invoice->approve());

        return true;
    }
}

<?php

namespace App\Modules\Invoices\Application\Api\Actions\ApproveInvoice;

use App\Modules\Invoices\Application\Api\Actions\ActionInterface;
use App\Modules\Invoices\Infrastructure\Http\Response\InvalidResponse;
use App\Modules\Invoices\Infrastructure\Http\Response\ResponseInterface;
use App\Modules\Invoices\Infrastructure\Http\Response\SuccessResponse;
use Illuminate\Http\Request;
use Illuminate\Log\Logger;

readonly class ApproveInvoiceAction implements ActionInterface
{
    public function __construct(
        private ApproveInvoiceService $approveInvoiceService,
        private Request $request,
        private Logger $logger
    ) {
    }

    public function execute(): ResponseInterface
    {
        try {
            $result = $this->approveInvoiceService->approveInvoice(
                new ApproveInvoiceDTO($this->request->route()?->parameters()['number'])
            );

            if ($result) {
                return new SuccessResponse(['message' => 'Invoice approved 👍']);
            }

            return new InvalidResponse(['message' => 'Invoice not approved 👎']);
        } catch (\Exception $e) {
            $this->logger->error($e);
            return new InvalidResponse(['message' => $e->getMessage()]);
        }
    }
}

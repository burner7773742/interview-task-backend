<?php

namespace App\Modules\Invoices\Application\Api\Actions\ApproveInvoice;

readonly class ApproveInvoiceDTO
{
    public function __construct(private string $invoiceNumber)
    {
    }

    public function getInvoiceNumber(): string
    {
        return $this->invoiceNumber;
    }
}

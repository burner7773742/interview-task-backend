<?php

namespace App\Modules\Invoices\Application\Api\Actions\RejectInvoice;

use App\Modules\Invoices\Application\Api\Actions\ActionInterface;
use App\Modules\Invoices\Infrastructure\Http\Response\InvalidResponse;
use App\Modules\Invoices\Infrastructure\Http\Response\ResponseInterface;
use App\Modules\Invoices\Infrastructure\Http\Response\SuccessResponse;
use Illuminate\Http\Request;
use Illuminate\Log\Logger;

readonly class RejectInvoiceAction implements ActionInterface
{
    public function __construct(
        private RejectInvoiceService $RejectInvoiceService,
        private Request $request,
        private Logger $logger
    ) {
    }

    public function execute(): ResponseInterface
    {
        try {
            $result = $this->RejectInvoiceService->RejectInvoice(
                new RejectInvoiceDTO($this->request->route()?->parameters()['number'])
            );

            if ($result) {
                return new SuccessResponse(['message' => 'Invoice rejected 👍']);
            }

            return new InvalidResponse(['message' => 'Invoice not rejected 👎']);
        } catch (\Exception $e) {
            $this->logger->error($e);
            return new InvalidResponse(['message' => $e->getMessage()]);
        }
    }
}

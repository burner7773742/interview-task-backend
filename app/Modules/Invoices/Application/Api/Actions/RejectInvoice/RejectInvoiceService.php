<?php

namespace App\Modules\Invoices\Application\Api\Actions\RejectInvoice;

use App\Modules\Approval\Api\ApprovalFacadeInterface;
use App\Modules\Approval\Api\Dto\ApprovalDto;
use App\Modules\Invoices\Domain\Repositories\InvoiceRepositoryInterface;
use App\Modules\Invoices\Infrastructure\Database\Exceptions\EntityNotFoundException;
use Ramsey\Uuid\Uuid;

readonly class RejectInvoiceService
{
    public function __construct(
        private InvoiceRepositoryInterface $invoiceRepository,
        private ApprovalFacadeInterface $approvalFacade
    ) {
    }

    /**
     * @throws EntityNotFoundException
     */
    public function RejectInvoice(RejectInvoiceDTO $RejectInvoiceDTO): bool
    {
        $invoice = $this->invoiceRepository->getByNumber($RejectInvoiceDTO->getInvoiceNumber());

        $this->approvalFacade->reject(
            new ApprovalDto(
                Uuid::fromString($invoice->getId()),
                $invoice->getStatus(),
                'Foo'
            )
        );

        $this->invoiceRepository->update($invoice->reject());

        return true;
    }
}

<?php

namespace App\Modules\Invoices\Application\Api\Actions\RejectInvoice;

readonly class RejectInvoiceDTO
{
    public function __construct(private string $invoiceNumber)
    {
    }

    public function getInvoiceNumber(): string
    {
        return $this->invoiceNumber;
    }
}

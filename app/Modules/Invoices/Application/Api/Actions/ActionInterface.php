<?php

namespace App\Modules\Invoices\Application\Api\Actions;

use App\Modules\Invoices\Infrastructure\Http\Response\ResponseInterface;

interface ActionInterface
{
    public function execute(): ResponseInterface;
}

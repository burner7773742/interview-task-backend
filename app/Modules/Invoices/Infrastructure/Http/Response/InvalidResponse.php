<?php

namespace App\Modules\Invoices\Infrastructure\Http\Response;

readonly class InvalidResponse implements \JsonSerializable, ResponseInterface
{
    public function __construct(private array $data)
    {
    }

    public function jsonSerialize(): array
    {
        return [
            'status' => false,
            ...$this->data,
        ];
    }
}

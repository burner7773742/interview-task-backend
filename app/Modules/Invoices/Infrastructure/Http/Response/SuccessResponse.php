<?php

namespace App\Modules\Invoices\Infrastructure\Http\Response;

readonly class SuccessResponse implements \JsonSerializable, ResponseInterface
{
    public function __construct(private array $data)
    {
    }

    public function jsonSerialize(): array
    {
        return [
            'status' => true,
            ...$this->data,
        ];
    }
}

<?php

namespace App\Modules\Invoices\Infrastructure\Database\Repositories;

use App\Modules\Invoices\Domain\Entities\Invoice;
use App\Modules\Invoices\Domain\Mappers\InvoiceMapper;
use App\Modules\Invoices\Domain\Repositories\InvoiceRepositoryInterface;
use App\Modules\Invoices\Infrastructure\Database\Exceptions\EntityNotFoundException;
use Illuminate\Database\Capsule\Manager;

readonly class InvoiceRepository implements InvoiceRepositoryInterface
{
    private const BASE_QUERY = 'SELECT
                    invoices.id as invoice_id,
                    invoices.number as invoice_number,
                    invoices.date as invoice_date,
                    invoices.due_date as invoice_due_date,
                    invoices.company_id as invoice_company_id,
                    invoices.status as invoice_status,
                    invoices.created_at as invoice_created_at,
                    invoices.updated_at as invoice_updated_at,
                    companies.id as company_id,
                    companies.name as company_name,
                    companies.street as company_street,
                    companies.city as company_city,
                    companies.zip as company_zip,
                    companies.phone as company_phone,
                    companies.email as company_email,
                    companies.created_at as company_created_at
                FROM invoices
                LEFT JOIN companies ON invoices.company_id = companies.id';

    public function __construct(private Manager $manager, private InvoiceMapper $mapper)
    {
    }

    /**
     * @return Invoice[]
     * @throws \Exception
     */
    public function getAll(): array
    {
        $result = $this->manager
            ->getConnection('mysql')
            ->select(self::BASE_QUERY);

        return array_map(
            fn(\stdClass $row) => $this->mapper->map($row, $this->fetchProducts($row->invoice_id)),
            $result
        );
    }

    private function fetchProducts(string $invoiceId): array
    {
        return $this->manager
            ->getConnection('mysql')
            ->select('
                SELECT
                    product.name as product_name,
                    invoice_product_lines.quantity as product_quantity,
                    product.price as product_price,
                    product.currency as product_currency
                FROM invoice_product_lines
                LEFT JOIN products product on product.id = invoice_product_lines.product_id
                WHERE invoice_product_lines.invoice_id = :invoiceId', [
                'invoiceId' => $invoiceId
            ]);
    }

    /**
     * @throws EntityNotFoundException
     * @throws \Exception
     */
    public function getByNumber(string $invoiceNumber): Invoice
    {
        $result = $this->manager
            ->getConnection('mysql')
            ->select(self::BASE_QUERY . ' WHERE number = :number LIMIT 1',
                [':number' => $invoiceNumber]
            );

        if (0 === count($result)) {
            throw new EntityNotFoundException(sprintf('Invoice number "%s" not found.', $invoiceNumber));
        }

        return $this->mapper->map($result[0], $this->fetchProducts($result[0]->invoice_id));
    }

    public function update(Invoice $invoice): void
    {
        $this->manager
            ->getConnection('mysql')
            ->update('
                UPDATE invoices
                SET date       = :date,
                    due_date   = :dueDate,
                    company_id = :companyId,
                    status     = :status,
                    updated_at = :updatedAt
                WHERE id = :id
                ', [
                'id' => $invoice->getId(),
                'date' => $invoice->getDate()->format('Y-m-d'),
                'dueDate' => $invoice->getDueDate()->format('Y-m-d'),
                'companyId' => $invoice->getCompany()->getId(),
                'status' => $invoice->getStatus()->value,
                'updatedAt' => (new \DateTime())->format('Y-m-d H:i:s'),
            ]);
    }
}
